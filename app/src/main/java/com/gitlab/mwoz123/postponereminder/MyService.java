package com.gitlab.mwoz123.postponereminder;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.CalendarContract;

public class MyService extends BroadcastReceiver {
    public MyService() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(CalendarContract.ACTION_EVENT_REMINDER)) {
            //Do Something Here to get EVENT ID
            Uri uri = intent.getData();

            String alertTime = uri.getLastPathSegment();

            String selection = CalendarContract.CalendarAlerts.ALARM_TIME + "=?";

            Cursor cursor = context.getContentResolver().query(CalendarContract.CalendarAlerts.CONTENT_URI_BY_INSTANCE, new String[]{CalendarContract.CalendarAlerts.EVENT_ID},selection,new String[]{alertTime}, null);
            String eventID = uri.getLastPathSegment();

            System.out.println(cursor);


//            long eventID = 208;
//            Uri uri2 = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, Long.valueOf(eventID));
            Intent intent2 = new Intent(Intent.ACTION_EDIT)
                    .setData(uri)
                    .putExtra(CalendarContract.Events.TITLE, "My New Title");
            context.startActivity(intent2);
        }
    }


}
